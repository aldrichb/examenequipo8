from django.db import models
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import post_save


# Create your models here.
# usuario examenray

CHOICES_GENERO = (
    ("Masculino", "M"),
    ("Femenino", "F")
)

class Persona(models.Model):
    nameuser = models.CharField(max_length=64, default='pepe')
    genero = models.CharField(max_length=16, choices=CHOICES_GENERO)
    email = models.CharField(max_length=100, default='example@gmail.com')

    def __str__ (self):
        return self.name
    
    #CLASS TASKS
class Task(models.Model):
    servicename = models.CharField(max_length=200)
    taskname = models.CharField(max_length=100)
    date = models.DateField(auto_now_add=True, auto_now=False)
    status = models.BooleanField(default=True)
    description = models.TextField(blank=True)
    product = models.CharField(max_length=100, default='telefono')
        
    def __str__(self):
        return self.servicename
    
class Product (models.Model):
    productname = models.CharField(max_length=64, default='laptop hp')
    description = models.CharField(max_length=100, blank=True)
    price = models.IntegerField(default=0)
    quantity = models.IntegerField(default=0)

    def __str__ (self):
        return self.productname







