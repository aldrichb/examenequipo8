from django.urls import path 
from core import views 

app_name = "core"

urlpatterns = [
    #### Task urls ####
    path('list/task/', views.ListTask.as_view(), name="list_t"),
    path('new/task/', views.NewTask.as_view(), name="create_t"),
    path('detail/task/<int:pk>/', views.DetailTask.as_view(), name="detail_t"),
    path('update/task/<int:pk>/', views.UpdateTask.as_view(), name="update_t"),
    path('delete/task/<int:pk>/', views.DeleteTask.as_view(), name="delete_t"),

    #### Persona urls ####
    path('list/persona/', views.ListPersona.as_view(), name="list_p"),
    path('new/persona/', views.NewPersona.as_view(), name="create_p"),
    path('detail/persona/<int:pk>/', views.DetailPersona.as_view(), name="detail_p"),
    path('update/persona/<int:pk>/', views.UpdatePersona.as_view(), name="update_p"),
    path('delete/persona/<int:pk>/', views.DeletePersona.as_view(), name="delete_p"),

    #### Product urls ####
    path('list/product/', views.ListProduct.as_view(), name="list_pr"),
    path('new/product/', views.NewProduct.as_view(), name="create_pr"),
    path('detail/product/<int:pk>/', views.DetailProduct.as_view(), name="detail_pr"),
    path('update/product/<int:pk>/', views.UpdateProduct.as_view(), name="update_pr"),
    path('delete/product/<int:pk>/', views.DeleteProduct.as_view(), name="delete_pr"),

]


   

