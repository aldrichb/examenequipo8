from django.db.models.query import QuerySet
from django.shortcuts import render
from django.views import generic
from django.urls import reverse_lazy

# Create your views here.

from core import models
from core import forms


# # # # crud persona # # # #
# # # C R E A T E # # #
class NewPersona(generic.CreateView):
    template_name = "core/create_p.html"
    model = models.Persona
    form_class = forms.NewPersonaForm
    success_url = reverse_lazy("core:list_p")
# # # R E T R I E V E # # #
# # list
class ListPersona(generic.ListView):
    template_name = "core/list_p.html"
    queryset = models.Persona.objects.all()
# # detail
class DetailPersona(generic.DetailView):
    template_name = "core/detail_p.html"
    model = models.Persona

# # # U P D A T E # # #
class UpdatePersona(generic.UpdateView):
    template_name = "core/update_p.html"
    model = models.Persona
    form_class = forms.UpdatePersonaForm
    success_url = reverse_lazy("core:list_p")
# # # D E L E T E # # #
class DeletePersona(generic.DeleteView):
    template_name = "core/delete_p.html"
    model = models.Persona
    success_url = reverse_lazy("core:list_p")

############## CRUD TASK ######################

class NewTask(generic.CreateView):
    template_name = "core/create_t.html"
    model = models.Task
    form_class = forms.NewTaskForm
    success_url = reverse_lazy("core:list_t")
# # # R E T R I E V E # # #
# # list
class ListTask(generic.ListView):
    template_name = "core/list_t.html"
    queryset = models.Task.objects.all()
# # detail
class DetailTask(generic.DetailView):
    template_name = "core/detail_t.html"
    model = models.Task

# # # U P D A T E # # #
class UpdateTask(generic.UpdateView):
    template_name = "core/update_t.html"
    model = models.Task
    form_class = forms.UpdateTaskForm
    success_url = reverse_lazy("core:list_t")
# # # D E L E T E # # #
class DeleteTask(generic.DeleteView):
    template_name = "core/delete_t.html"
    model = models.Task
    success_url = reverse_lazy("core:list_t")

############## CRUD PRODUCT ######################

class NewProduct(generic.CreateView):
    template_name = "core/create_pr.html"
    model = models.Product
    form_class = forms.NewProductForm
    success_url = reverse_lazy("core:list_pr")
# # # R E T R I E V E # # #
# # list
class ListProduct(generic.ListView):
    template_name = "core/list_pr.html"
    queryset = models.Product.objects.all()
# # detail
class DetailProduct(generic.DetailView):
    template_name = "core/detail_pr.html"
    model = models.Product

# # # U P D A T E # # #
class UpdateProduct(generic.UpdateView):
    template_name = "core/update_pr.html"
    model = models.Product
    form_class = forms.UpdateProductForm
    success_url = reverse_lazy("core:list_pr")
# # # D E L E T E # # #
class DeleteProduct(generic.DeleteView):
    template_name = "core/delete_pr.html"
    model = models.Product
    success_url = reverse_lazy("core:list_pr")