from django import forms
from core import models
######### persona forms #########
class NewPersonaForm(forms.ModelForm):
    class Meta:
        model = models.Persona
        fields = [
            "nameuser",
            "genero",
            "email"
        ]
        widgets = {
            "nameuser": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu nombre"}),
            "genero": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "email": forms.TextInput(attrs={"type":"text", "class":"form-control"})
        }

class UpdatePersonaForm(forms.ModelForm):
    class Meta:
        model = models.Persona
        fields = [
            "nameuser",
            "genero",
            "email"
        ]
        widgets = {
            "nameuser": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu nombre"}),
            "genero": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "email": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
        }
  ############ task forms #########  
class NewTaskForm(forms.ModelForm):
    class Meta:
        model = models.Task
        fields = [
            "servicename",
            "taskname",
            "description",
            "product",
        ]
        widgets = {
            "servicename": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "taskname": forms.TextInput(attrs={"type":"select", "class":"form-control"}),
            "description": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "product": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            
        }

class UpdateTaskForm(forms.ModelForm):
    class Meta:
        model = models.Task
        fields = [
            "servicename",
            "taskname",
            "description",
            "product",
        ]
        widgets = {
            "servicename": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "taskname": forms.TextInput(attrs={"type":"select", "class":"form-control"}),
            "description": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "product": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            
        }
 ######## product forms ########
class NewProductForm(forms.ModelForm):
    class Meta:
        model = models.Product
        fields = [
            "productname",
            "description",
            "price",
            "quantity",
        ]
        widgets = {
            "productname": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "description": forms.Textarea(attrs={"type":"text", "class":"form-control", "row":3}),
            "price": forms.NumberInput(attrs={"type":"number", "class":"form-control"}),
            "quantity": forms.NumberInput(attrs={"type":"number", "class":"form-control"}),
            
        }

class UpdateProductForm(forms.ModelForm):
    class Meta:
        model = models.Product
        fields = [
            "productname",
            "description",
            "price",
            "quantity",
        ]
        widgets = {
            "productname": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "description": forms.Textarea(attrs={"type":"text", "class":"form-control", "row":3}),
            "price": forms.NumberInput(attrs={"type":"number", "class":"form-control"}),
            "quantity": forms.NumberInput(attrs={"type":"number", "class":"form-control"}),
            
        }